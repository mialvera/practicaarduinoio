#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>
#include <funciones.h>

#include "arduino-serial-lib.h"

// #define TOTALMUESTRA 12

// float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{	

	float temperaturas[TOTALMUESTRA];
	float humedades[TOTALMUESTRA];

	int fd = -1;
	int baudrate = 9600;  // default

	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
		
	serialport_flush(fd);
	
	int i = 0;
	
	char cT = 't';
	char cH = 'h';
	char on = '1';
	char off = '0';
	
	// Toma de datos por un minuto
	// Como cada muestreo dura 5 segundos
	// Se tienen un total de 12 entes en la muestra.

	while (i < TOTALMUESTRA) {
		sleep(5);
		write(fd, &on, 1);

		write(fd, &cT, 1);	
		usleep(5000);	
		
		int temp = 0;
		
		int n = read(fd, &temp, 1);
				
		while (n == 0) {
			n = read(fd, &temp, 1);
        }

		temperaturas[i] = (float)temp;
		printf("Temp %d: %.2f\n", (i + 1), temperaturas[i]);
		write(fd, &cH, 1);	
		usleep(5000);	
		
		int hum = 0;
		
		n = read(fd, &hum, 1);
		while (n == 0) {
			n = read(fd, &hum, 1);
        }		
		
		humedades[i] = (float)hum;
		printf("Hum %d: %.2f\n\n", (i + 1),humedades[i]);
		write(fd, &off, 1);				

		i++;
	}

	float *minMaxTemperatura = calcularMinMax(temperaturas);

	float *minMaxHumedad = calcularMinMax(humedades);

	// Temperatura máxima y mínima

	float minTemp = minMaxTemperatura[0];

	float maxTemp = minMaxTemperatura[1];

	// Humedad máxima y mínima.

	float minHum = minMaxHumedad[0];

	float maxHum = minMaxHumedad[1];

	// Temperatura Promedio

	float meanTemp = calcularMedia(temperaturas);

	// Humedad Promedio

	float meanHum = calcularMedia(humedades);

	// Desviación Estándar Temperatura

	float sdTemperatura = calculateSD(temperaturas, meanTemp);

	// Desviación Estándar Humedades

	float sdHumedades = calculateSD(humedades, meanHum);

	printf("Acerca de Temperaturas:\n");
	printf("La temperatura mínima es: %.2f grados\n", minTemp);
	printf("La temperatura máxima es: %.2f grados\n", maxTemp);
	printf("La temperatura promedio es: %.2f grados\n", meanTemp);
	printf("La desviación estándar de temperaturas es: %.2f\n", sdTemperatura);	

	printf("\nAcerca de Humedades:\n");
	printf("La humedad mínima es: %.2f por ciento\n", minHum);
	printf("La humedad máxima es: %.2f por ciento\n", maxHum);
	printf("La humedad promedio es: %.2f por ciento\n", meanHum);
	printf("La desviación estándar de humedades es: %.2f\n", sdHumedades);

	close( fd );
	return 0;	
}

/* Ejemplo para calcular desviacion estandar y media */
// float calculateSD(float data[])
// {
//     float sum = 0.0, mean, standardDeviation = 0.0;

//     int i;

//     for(i = 0; i < 10; ++i)
//     {
//         sum += data[i];
//     }

//     mean = sum/10;

//     for(i = 0; i < 10; ++i)
//         standardDeviation += pow(data[i] - mean, 2);

//     return sqrt(standardDeviation / 10);
// }


//float *temperatura(){



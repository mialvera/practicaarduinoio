#include <funciones.h>
#include <stdlib.h>
#include <math.h>

float calcularMedia(float data[]) {

	float suma;

	suma = 0;

	if (TOTALMUESTRA > 0) {

		for (int i = 0; i < TOTALMUESTRA; i++) {

			suma += data[i];

		}

		return suma / TOTALMUESTRA;

	}
	else {
		return 0;
	}

}

float * calcularMinMax(float data[]) {

	float *minMax = malloc(sizeof(float) * 2);

	float min, max, temp;

	if (TOTALMUESTRA > 0) {

		min = max = data[0];

		if (TOTALMUESTRA > 1) {

			for (int i = 1; i < TOTALMUESTRA; i++) {

				
				temp = data[i];

				if (temp < min) {
					min = temp;
				}

				if (temp > max) {
					max = temp;
				}

			}
		}
	}
	else {
		min = max = 0;
	}

	minMax[0] = min;
	minMax[1] = max;

	return minMax;

}

float calculateSD(float data[], float mean) {
    float standardDeviation = 0.0;

    int i;

    for(i = 0; i < TOTALMUESTRA; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    if (TOTALMUESTRA > 0) {
    	return sqrt(standardDeviation / TOTALMUESTRA);
    }
    else {
    	return standardDeviation;
    }
}